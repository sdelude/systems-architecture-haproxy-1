#!/bin/bash
# This script will take down the erecruit maintenance page
if [ ! -L /etc/haproxy/haproxy.cfg ]
then
    echo "/etc/haproxy/haproxy.cfg is not a symlink! Stopping."
    exit -1
fi
rm /etc/haproxy/haproxy.cfg
ln -s /etc/haproxy/haproxy_active.cfg /etc/haproxy/haproxy.cfg
ls -la /etc/haproxy/haproxy.cfg
service haproxy restart
